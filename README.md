This is a training project using the All-In-One (AIO) archetype from Alfresco SDK 3.0 Beta. 

Run with `mvn clean install -DskipTests=true alfresco:run` or `./run.sh`